lib.fit package
===============

.. automodule:: lib.fit

lib.fit.params module
---------------------

.. automodule:: lib.fit.params
   :members:
   :undoc-members:
   :show-inheritance:
